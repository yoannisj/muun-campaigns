var breakpoints = require('data.json').breakpoints;
require('slick');

var $carousels = $('.carousel');

$carousels.each(function(i, el) {
  var $carousel = $(el),
    $items = $carousel.find('.carousel-items');

  $carousel.addClass('has-carousel');
  $items.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: false,
    dots: false,
    infinite: false,
    centerMode: true,
    centerPadding: "15%",
    adaptiveHeight: false,
    swipeToSlide: true,
    draggable: false,
    touchMove: true,
    edgeFriction: 0.8,
    focusOnSelect: false,
    touchThreshold: 6,

    mobileFirst: true,
    responsive: [{
      breakpoint: breakpoints.sm,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        initialSlide: 1,
        centerPadding: "7%",
      }
    }, {
      breakpoint: breakpoints.xl,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 5,
        initialSlide: 2,
        centerPadding: "5%"
      }
    }]
  });

  $items.on('click', '.carousel-item', function(ev) {
    var index = $(ev.currentTarget).index();
    $items.slick('slickGoTo', index);
  });
});