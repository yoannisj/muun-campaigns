// load embed player api asynchronously
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var $window = $(window);
var $body = $('body');

// iOS sniffing
var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) )

// when youtube ebed player API has loaded and is ready
window.onYouTubeIframeAPIReady = function() {
  // listen to video links and create player when clicked
  $body.on('click.ytplay', '.js-ytplay', onYTControlClick);
};

// when clicking on a youtube video control
function onYTControlClick(ev) {
  ev.preventDefault();
  var $ctrl = $(ev.currentTarget),
    ytid = $ctrl.attr('data-youtube-id'),
    target = $ctrl.attr('data-target'),
    $player = $('#' + target),
    player = $player.data('ytPlayer');

  // if player was initialised before
  if (player && !$player.data('ytPlayerLoading')) {
    startYTPlayer(player, $player);
  }

  // else, create new youtube player
  else {
    $player.data('ytPlayerLoading', true);
    player = createYTPlayer(ytid, $player);
  }
}

// create a new youtube player and return it
function createYTPlayer(videoId, $player) {
  var elem = $player.find('.video-iframe')[0];

  return new YT.Player(elem, {
    width: $window.width(),
    height: $window.height(),
    videoId: videoId,
    playerVars: {
      enablejsapi: 1,
      origin: window.location.origin,
      autoplay: 1,
      loop: 0,
      modestbranding: 0,
      showinfo: 0,
      controls: 2,
      fs: 0
    },
    events: {
      'onReady': onYTPlayerReady,
      'onError': onYTPlayerError,
      'onStateChange': onYTPlayerStateChange
    }
  });
}

// when new youtube player is ready
function onYTPlayerReady(ev) {
  var player = ev.target,
    $iframe = $( player.getIframe() ),
    $player = $iframe.closest('.video-player');

  // store ref to player for later access
  $player.data('ytPlayer', player);
  $player.data('ytPlayerLoading', false);

  // set custom playback options
  player.setPlaybackQuality('hd1080');
  // start the player
  startYTPlayer(ev.target, $player, true);
}

function onYTPlayerError(ev) {
  var player = ev.target,
    $iframe = $( player.getIframe() ),
    $player = $iframe.closest('.video-player');

  // close player
  closeYTPlayer(player, $player);
}

// timeout used to check if pausing or not
function onYTPlayerStateChange(ev) {
  // close on end
  if (ev.data == YT.PlayerState.ENDED) {
    closeOnYTStateChange(ev.target);
  }

  // wait on pause and close if not buffering
  else if (ev.data == YT.PlayerState.PAUSED) {
    // double-check if not just paused because of seekTo()
    setTimeout(function() {
      if (ev.target.getPlayerState() == YT.PlayerState.PAUSED) {
        closeOnYTStateChange(ev.target, true);
      }
    }, 8);
  }
}

function closeOnYTStateChange(player, animate) {
  var $iframe = $( player.getIframe() ),
    $player = $iframe.closest('.video-player'),
    closefn = animate ? animCloseYTPlayer : closeYTPlayer;

  closefn(player, $player);
}

// start playing video and open player
function startYTPlayer(player, $player, firstTime) {
  // fade-in player
  $player.animate({ opacity: 1 }, 350, 'linear');

  // start video
  if (!isMobile || (isMobile && !firstTime)) player.playVideo();
  $body.addClass('has-video-player');
  $player.addClass('is-active');

  // listen for 'close'
  $player.on('click.ytplay', '.video-close', function(ev) {
    ev.preventDefault();
    player.pauseVideo();
  });
}

// fade player out before stopping
function animCloseYTPlayer(player, $player) {
  $player.animate({ opacity: 0 }, 350, 'linear', function() {
    closeYTPlayer(player, $player);
  });
}

// stop playing video and close player
function closeYTPlayer(player, $player, animate) {
  $player.removeClass('is-active');
  $player.off('.ytplay');
  $body.removeClass('has-video-player');
}