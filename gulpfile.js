var gulp = require('gulp');
var slurp = require('./gulp/slurp');

slurp.load('templates');
slurp.load('stylesheets');
slurp.load('javascripts');
slurp.load('externals',['stylesheets', 'javascripts']);
slurp.load('assets', ['stylesheets', 'javascripts', 'externals']);

gulp.task('build', ['templates', 'assets']);
gulp.task('default', ['build']);