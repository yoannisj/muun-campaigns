var slurp = require('../slurp');

function compileTemplates() {
  return slurp.src('templates')
    .pipe(slurp.dest('templates'));
}

// exports
module.exports = function(done) {

  if (slurp.env.watch)
  {
    // reload on compilation
    slurp.plugins.livereload.listen();

    // compile on file changes
    slurp.watch('templates', function(ev) {
      return compileTemplates()
        .pipe(slurp.plugin('livereload'));

    });
  }

  return compileTemplates();

};