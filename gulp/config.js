module.exports = function(env) {

  return {
    paths: {
      src: './src',
      dest: './www',
      bower: './bower_components',
      public: 'http://campaign.muun.local',
      data: '<%= paths.src %>/data'
    },

    tasks: {

      'templates': {
        base: {
          src: './templates',
          dest: './'
        },
        files: './*.html'
      },

      'assets': {
        base: {
          src: './assets/'
        },
        files: [
          '**/*',
          '!styles',
          '!styles/**/*',
          '!scripts',
          '!scripts/**/*'
        ]
      },

      'stylesheets': {
        base: {
          src: './assets/styles',
          dest: './css'
        },
        files: {
          main: './*.scss',
          watch: './**/*.scss'
        }
      },

      'javascripts': {
        base: {
          src: './assets/scripts',
          dest: './js'
        },
        files: {
          main: './*.js',
          watch: './**/*.js'
        }
      },

      'externals': {
        files: [
          '<%= paths.bower %>/jquery/dist/jquery.js',
          '<%= paths.src %>/<%= tasks.javascripts.base.src %>/vendor/modernizr.js',
          '<%= paths.src %>/<%= tasks.javascripts.base.src %>/vendor/slick.js'
        ],
        base: {
          src: '../',
          dest: '<%= tasks.javascripts.base.dest %>/vendor'
        }
      }
    }

  };

};